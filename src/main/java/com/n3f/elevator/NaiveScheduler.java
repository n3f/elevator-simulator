package com.n3f.elevator;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

/**
 * Scheduler that enqueues each call request to the elevator that is closest to the requesting floor.
 *
 * Tie breaks are at your discretion. Although not required by your algorithm, other schedulers may also require the
 * requested destination floor to make an informed decision.
 */
public class NaiveScheduler implements Scheduler {
  @Autowired
  Collection<Elevator> elevators;

  @Override
  public Collection<ElevatorAction> schedule(Requests requests) {
    // Operate on copy of requests and create sorted set
    try {
      Requests r = new Requests(requests);
      return _schedule(r);
    }
    catch (InvalidRequestException ire){
      return new ArrayList<>();
    }
  }

  /**
   * Find the request closest to an elevator and add the request as an action
   *
   * @param requests
   * @return
   */
  private Collection<ElevatorAction> _schedule(Requests requests) {
    List<ElevatorAction> actions = new ArrayList<>();

    Collection<Elevator> remainingElevators = new ArrayList<>();
    remainingElevators.addAll(elevators);

    // The loop needs to find the closest request for each elevator and then remove
    // the "solved" action from the equation.  The loop finishes when either all the
    // elevators are taken, or all the requests are fulfilled.
    while (remainingElevators.size() > 0 && requests.getRequests().size() > 0) {
      // Find the ElevatorAction with the minimum distance (closest elevator to request)
      // TODO: suboptimal O((m*n)^2) brute-force algorithm may be improvable.
      ElevatorAction action = remainingElevators.stream()
          .flatMap(elevator -> requests.getRequests().stream()
              .map(r -> new ElevatorAction(elevator, r)))
          .min(Comparator.comparing(ElevatorAction::getDistance))
          .orElse(null);

      if (action == null) break; // No action found -- all done

      // Add/remove the solved elevator and request to/from solution set/problem set,
      remainingElevators.remove(action.getElevator());
      requests.fulfillRequests(Arrays.asList(action));
      actions.add(action);
    }

    return actions;
  }
}
