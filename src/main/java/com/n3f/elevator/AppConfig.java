package com.n3f.elevator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Spring configuration / dependency-injection vars
 */
@Configuration
public class AppConfig {

  @Value("${elevators.count:10}")
  private int elevatorCount;

  @Value("${floors.count:100}")
  public int floorCount;

  @Value("${passengers.count:2500}")
  public int passengerCount;

  @Bean
  public Collection<Elevator> getElevators() {
    Collection<Elevator> elevators = new ArrayList<>();
    for (int i=0; i<elevatorCount; i++) {
      elevators.add(new Elevator());
    }
    return elevators;
  }

  @Bean
  public Scheduler getScheduler() {
    return new NaiveScheduler();
  }
}
