package com.n3f.elevator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Range;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to manage simulator requests
 */
@NoArgsConstructor
class Requests {
  @Getter
  protected Collection<Request> requests = new ArrayList<>();

  /**
   * Constructor
   *
   * @param requests
   */
  Requests(Collection<Request> requests) throws InvalidRequestException {
    for (Request request:requests) {
      this.requests.add(new Request(request.getRequestFloor(), request.getDestinationFloor()));
    }
  }

  /**
   * Copy constructor
   *
   * @param original
   */
  Requests(Requests original) throws InvalidRequestException {
    this(original.getRequests());
  }

  /**
   * Take a series of elevator actions and fulfill outstanding requests
   *
   * @param actions
   */
  void fulfillRequests(Collection<ElevatorAction> actions) {
    for (ElevatorAction action : actions) {
      Request.Direction direction = action.getRequest().getDirection();
      Range<Integer> actionRange = action.getRequest().getRange();
      List<Request> temp = requests.stream().filter(request -> {
        if (request.getDirection() != direction) return false;
        if (actionRange.containsRange(request.getRange())) return true;
        return false;
      }).collect(Collectors.toList());
      requests.removeAll(temp);
      action.getElevator().setCurrentFloor(action.getRequest().getDestinationFloor());
    }
  }
}
