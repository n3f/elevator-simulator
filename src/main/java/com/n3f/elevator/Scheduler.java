package com.n3f.elevator;

import java.util.Collection;

/**
 * Define an interface for the Scheduler.
 * <p>
 * Scheduler should take requests and return the actions that the elevator should perform. Never return more actions
 * than there are elevators, and each elevator should only have one tasking.
 */
public interface Scheduler {

  Collection<ElevatorAction> schedule(Requests requests);

}
