package com.n3f.elevator;

import lombok.*;

import java.util.UUID;

/**
 * Bean for managing an Elevator
 */
@Getter
@Setter
@EqualsAndHashCode
public class Elevator {
  private int currentFloor;
  @Setter(AccessLevel.PRIVATE) @NonNull private UUID id;

  public Elevator() {
    id = UUID.randomUUID();
    currentFloor = 0;
  }
}