package com.n3f.elevator;

public class InvalidRequestException extends Exception {
  public InvalidRequestException(String message) {
    super(message);
  }
}
