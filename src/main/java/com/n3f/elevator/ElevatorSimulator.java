package com.n3f.elevator;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Collection;

/**
 * Spring/commandline application to run an elevator simulator
 */
@SpringBootApplication
public class ElevatorSimulator {
  private static final Logger log = LoggerFactory.getLogger(ElevatorSimulator.class);

  @Autowired
  private Collection<Elevator> elevators;

  @Autowired
  private Scheduler scheduler;

  @Autowired
  private AppConfig appConfig;

  Requests requests = new Requests();

  /**
   * Where the magic starts...
   */
  public void start() throws Exception {
    log.info("number of elevators: {}", elevators.size());
    logStatus(0,0,0);
    try {
      while (true) {
        int start = requests.getRequests().size();
        generateRequests();
        int created = requests.getRequests().size() - start;
        requests.fulfillRequests(scheduler.schedule(requests));
        int fulfilled = start + created - requests.getRequests().size();
        logStatus(created, fulfilled, requests.getRequests().size());
        Thread.sleep(1000);
      }
    }
    catch (Exception ex) {
      throw ex;
    } finally {

    }
  }

  /**
   * Create some more requests...
   */
  private void generateRequests() {
    for (int i = 0; i <= RandomUtils.nextInt(0, appConfig.passengerCount); i++) {
      try {
        requests.getRequests().add(new Request(RandomUtils.nextInt(0, elevators.size()),
                                               RandomUtils.nextInt(0, elevators.size())));
      } catch (InvalidRequestException ire) {
      }
    }
  }

  /**
   * Give some idea as to what is going on...
   */
  private void logStatus(int created, int fulfilled, int remaining) {
    log.info("Requests created: {}", created);
    log.info("Requests fulfilled: {}", fulfilled);
    log.info("Requests remaining: {}", remaining);
    for (Elevator e:elevators) {
      log.info("Elevator({})@Floor {}", e.getId(), e.getCurrentFloor());
    }
  }

  /**
   * App startup
   * @param args
   */
  public static void main(String args[]) {
    SpringApplication.run(ElevatorSimulator.class, args).close();
  }

  /**
   * App startup
   * @param ctx
   * @return
   */
  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      start();
    };
  }
}
