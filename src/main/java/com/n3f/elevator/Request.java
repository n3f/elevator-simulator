package com.n3f.elevator;

import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.Range;

/**
 * Bean for tracking elevator call requests
 */
@Value
class Request {
  private int requestFloor;
  private int destinationFloor;

  enum Direction {UP,DOWN};

  Request(int requestFloor, int destinationFloor) throws InvalidRequestException {
    if (requestFloor == destinationFloor) {
      throw new InvalidRequestException("Floors are the same");
    }
    this.requestFloor = requestFloor;
    this.destinationFloor = destinationFloor;
  }

  /**
   * Going up or down?
   *
   * @return Direction
   */
  Direction getDirection(){
    if (requestFloor > destinationFloor) return Direction.UP;
    else return Direction.DOWN;
  }

  /**
   * Convenience method for determining if a request matches an ElevatorAction
   *
   * @return range of the floors to travel to fulfill request
   */
  Range<Integer> getRange() {
    return Range.between(requestFloor, destinationFloor);
  }
}
