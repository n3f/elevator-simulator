package com.n3f.elevator;

import lombok.Value;
import lombok.experimental.PackagePrivate;

/**
 * Bean for sending ElevatorActions
 */
@Value
@PackagePrivate
class ElevatorAction {
  Elevator elevator;
  Request request;

  /**
   * Find the travel distance for an elevator to fulfill a request
   *
   * @return
   */
  public int getDistance(){
    return Math.abs(elevator.getCurrentFloor() - request.getRequestFloor());
  }
}
