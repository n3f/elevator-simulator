package com.n3f.elevator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@RunWith(SpringJUnit4ClassRunner.class)
public class NaiveSchedulerTest {

  private NaiveScheduler scheduler;

  Collection<Elevator> elevators;

  @Before
  public void setUp() throws Exception {
    scheduler = new NaiveScheduler();
    elevators = new ArrayList<Elevator>();
    ReflectionTestUtils.setField(scheduler, "elevators", elevators);
  }

  @Test
  public void test_schedule_closest() throws InvalidRequestException {
    Elevator zero = new Elevator();
    Elevator first = new Elevator();
    first.setCurrentFloor(1);

    elevators.add(zero);
    elevators.add(first);

    assertEquals(2, elevators.size());
    assertEquals(0, zero.getCurrentFloor());
    assertEquals(1, first.getCurrentFloor());

    Requests requests = new Requests(Arrays.asList(new Request(2,0)));
    Collection<ElevatorAction> actions = scheduler.schedule(requests);
    ElevatorAction[] actionsArray = actions.toArray(new ElevatorAction[actions.size()]);
    assertEquals(1, actionsArray.length);
    assertEquals(first, actionsArray[0].getElevator());
    assertEquals(0, actionsArray[0].getElevator().getCurrentFloor());
  }


  @Test
  public void test_scheduleMultipleRequests() throws InvalidRequestException {
    Elevator zero = new Elevator();
    Elevator first = new Elevator();
    first.setCurrentFloor(1);

    elevators.add(zero);
    elevators.add(first);

    Request r1 = new Request(3, 4);
    Request r2 = new Request(1, 3);
    Request r3 = new Request(6, 7);

    Requests requests = new Requests(Arrays.asList(r1, r2, r3));
    Collection<ElevatorAction> actions = scheduler.schedule(requests);
    requests.fulfillRequests(actions);
    assertEquals(1, requests.getRequests().size());
    assertEquals(2, actions.size());
    for (ElevatorAction ea : actions) {
      if (ea.getElevator().equals(zero)) {
        assertEquals(4, zero.getCurrentFloor());
      }
      if (ea.getElevator().equals(first)) {
        assertEquals(3, first.getCurrentFloor());
      }
    }
  }
}